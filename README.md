## Submission skeleton
```
- code               # training and evaluation code
- resources
  |__ embeddings.vec # my embeddings
- README.md          # this file
- Homework_2_nlp.pdf # the slides presenting this homework
- report.pdf         # report
```
